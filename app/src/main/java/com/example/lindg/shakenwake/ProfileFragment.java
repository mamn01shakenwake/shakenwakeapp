package com.example.lindg.shakenwake;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class ProfileFragment extends Fragment {

    private ListView list;
    public int counter;
    Bundle bundle = new Bundle();

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        list = (ListView) v.findViewById(R.id.list);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0){counter=0;}
                if (position == 1){counter=1;}
                if (position == 2){counter=2;}
                if (position == 3){counter=3;}
                if (position == 4){counter=4;}
                if (position == 5){counter=5;}


                FragmentManager fm = getFragmentManager();
                ProfileDialogFragment dialogFragment = new ProfileDialogFragment();
                bundle.putInt("key", counter);
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Dialog Fragment");
            }
        });

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(), R.array.ArrayProfiles, android.R.layout.simple_list_item_1);
        list.setAdapter(adapter);

        return v;
    }
}
