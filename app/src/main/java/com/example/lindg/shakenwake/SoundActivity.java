package com.example.lindg.shakenwake;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.lindg.shakenwake.alarm.AlarmReceiver;

public class SoundActivity extends AppCompatActivity implements SensorEventListener {

    private final float SHAKE_THRESHHOLD = 400;
    private final double SENSOR_THRESHHOLD = 7;
    private final float alph = 0.8f;
    public SensorManager mSensorManager;
    public Sensor Accelerometer;
    protected float[] gravSensorVals;

    private float[] lastEventVals;
    private long lastDetection;
    private int count, alarmID;
    private SoundPool soundPool;
    private Intent intent;
    private Window window;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound);
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        alarmID = soundPool.load(this, R.raw.siren, 1);
        lastDetection = System.currentTimeMillis();
        lastEventVals = new float[3];
        count = 0;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_RING);


        intent = getIntent();
        window = getWindow();

        //Unlock phone, turn on screen during alarms
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        SystemClock.sleep(500);
        soundPool.play(alarmID, 1, 1, 1, -1, 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
        count = 0;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        soundPool.pause(alarmID);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if ((System.currentTimeMillis() - lastDetection) > SHAKE_THRESHHOLD) {
                double[] accVals = new double[3];
                float[] eventVals = event.values.clone();
                gravSensorVals = lowPass(eventVals, gravSensorVals);

                accVals = subtractVector3(eventVals, gravSensorVals);
                double[] diffVals = subtractVector3(eventVals, lastEventVals);

                double speed = Math.sqrt(Math.pow(accVals[0], 2) + Math.pow(accVals[1], 2) + Math.pow(accVals[2], 2));
//                double diff = Math.sqrt(Math.pow(diffVals[0], 2) + Math.pow(diffVals[1], 2) + Math.pow(diffVals[2], 2));
                double diff = Math.abs(diffVals[0] + diffVals[1] + diffVals[2]);
                String message = "";

                if (speed > SENSOR_THRESHHOLD && diff > SENSOR_THRESHHOLD) {
                    count++;
                    lastDetection = System.currentTimeMillis();
                    lastEventVals = eventVals;
                }
                if (count > 10) {
                    soundPool.pause(alarmID);
                    soundPool.release();
                    message = "Alarm has been terminated";
                    AlarmReceiver.completeWakefulIntent(intent);
                    finish();
                } else {
                    message = "Alarm";
                }


            } else {
                return;
            }
        } else {
            return;
        }
    }

    protected float[] lowPass(float[] input, float[] output) {
        if (output == null) return input;
        for (int i = 0; i < input.length; i++) {
            output[i] = alph * output[i] + (1 - alph) * input[i];
        }
        return output;
    }

    protected double[] subtractVector3(float[] one, float[] two) {
        double[] result = new double[3];
        result[0] = one[0] - two[0];
        result[1] = one[1] - two[1];
        result[2] = one[2] - two[2];
        return result;
    }
}