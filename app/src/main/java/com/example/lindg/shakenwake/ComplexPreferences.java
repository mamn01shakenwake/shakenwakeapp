package com.example.lindg.shakenwake;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.lindg.shakenwake.alarm.Alarm;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

public class ComplexPreferences {
    private static ComplexPreferences complexPreferences;
    private static Gson GSON = new Gson();
    private final Context context;
    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    Type typeOfObject = new TypeToken<Object>() {
    }.getType();

    private ComplexPreferences(Context context, String namePreferences, int mode) {
        this.context = context;
        if (namePreferences == null || namePreferences.equals("")) {
            namePreferences = "abhan";
        }
        preferences = context.getSharedPreferences(namePreferences, mode);
        editor = preferences.edit();
    }

    public static ComplexPreferences getComplexPreferences(Context context,
                                                           String namePreferences, int mode) {
        if (complexPreferences == null) {
            complexPreferences = new ComplexPreferences(context,
                    namePreferences, mode);
        }
        return complexPreferences;
    }

    public void putObject(String key, Object object) {
        if (object == null) {
            throw new IllegalArgumentException("Alarm is null");
        }
        if (key.equals("") || key == null) {
            throw new IllegalArgumentException("Key is empty or null");
        }
        editor.putString(key, GSON.toJson(object));
        Toast.makeText(context, "Alarm Added compPref", Toast.LENGTH_SHORT).show();
    }

    public void commit() {
        editor.commit();
    }

    public <T> T getObject(String key, Class<T> a) {
        String gson = preferences.getString(key, null);
        if (gson == null) {
            return null;
        } else {
            try {
                return GSON.fromJson(gson, a);
            } catch (Exception e) {
                throw new IllegalArgumentException("Object stored with key "
                        + key + " is instance of other class");
            }
        }
    }

    public ArrayList<Alarm> getAlarms() {
        Map<String, ?> map = preferences.getAll();
        ArrayList<Alarm> alarmList = new ArrayList<>();
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            if (entry.getValue().getClass().equals(Alarm.class)) {
                alarmList.add((Alarm) entry.getValue());
            }
        }
        return alarmList;
    }
}