package com.example.lindg.shakenwake;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.example.lindg.shakenwake.alarm.SetAlarmActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AlarmFragment extends Fragment implements View.OnClickListener {

    ListAlarmFragment listAlarmFragment;
    private int hourChoice, minuteChoice, currentHour, currentMinute;
    private int currentImageResource;
    private Spinner spinner;
    private TimePicker timePrompt;
    private ImageView imageView1;
    private String spinner_title = "Set time";
    private String profileChoice, profileName;
    private Button startAlarmButton;
    private Map<String, String> profiles = new HashMap<>();
    private Calendar calendar;

    public AlarmFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = calendar.get(Calendar.MINUTE);
    }

    public ListAlarmFragment makeListFragment() {

        listAlarmFragment = new ListAlarmFragment();
        return listAlarmFragment;
    }


    private void updateDisplay(TimePicker view, int hourOfDay, int minute) {
        hourChoice = hourOfDay;
        minuteChoice = minute;


        int value = hourOfDay;
        switch (value) {
            case 1:
                changeImage(currentImageResource, R.drawable.f1);
                currentImageResource = R.drawable.f1;
                break;
            case 2:
                //view.setBackgroundResource(R.drawable.frame_2);
                //currentImageResource = R.drawable.frame_2;
                //changeImage();
                changeImage(currentImageResource, R.drawable.f2);
                currentImageResource = R.drawable.f2;
                break;
            case 3:
                //view.setBackgroundResource(R.drawable.frame_3);
                changeImage(currentImageResource, R.drawable.f3);
                currentImageResource = R.drawable.f3;

                break;
            case 4:
                //view.setBackgroundResource(R.drawable.frame_4);
                changeImage(currentImageResource, R.drawable.f4);
                currentImageResource = R.drawable.f4;
                break;
            case 5:
                //view.setBackgroundResource(R.drawable.frame_5);
                changeImage(currentImageResource, R.drawable.f5);
                currentImageResource = R.drawable.f5;
                break;
            case 6:
                //view.setBackgroundResource(R.drawable.frame_6);
                changeImage(currentImageResource, R.drawable.f6);
                currentImageResource = R.drawable.f6;
                break;
            case 7:
                //view.setBackgroundResource(R.drawable.frame_7);
                changeImage(currentImageResource, R.drawable.f7);
                currentImageResource = R.drawable.f7;
                break;
            case 8:
                //view.setBackgroundResource(R.drawable.frame_8);
                changeImage(currentImageResource, R.drawable.f8);
                currentImageResource = R.drawable.f8;
                break;
            case 9:
                //view.setBackgroundResource(R.drawable.frame_9);
                changeImage(currentImageResource, R.drawable.f9);
                currentImageResource = R.drawable.f9;
                break;
            case 10:
                //view.setBackgroundResource(R.drawable.frame_10);
                changeImage(currentImageResource, R.drawable.f10);
                currentImageResource = R.drawable.f10;
                break;
            case 11:
                //view.setBackgroundResource(R.drawable.frame_11);
                changeImage(currentImageResource, R.drawable.f11);
                currentImageResource = R.drawable.f11;
                break;
            case 12:
                //view.setBackgroundResource(R.drawable.frame_12);
                changeImage(currentImageResource, R.drawable.f12);
                currentImageResource = R.drawable.f12;
                break;
            case 13:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f13);
                currentImageResource = R.drawable.f13;
                break;
            case 14:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f14);
                currentImageResource = R.drawable.f14;
                break;
            case 15:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f15);
                currentImageResource = R.drawable.f15;
                break;
            case 16:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f16);
                currentImageResource = R.drawable.f16;
                break;
            case 17:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f17);
                currentImageResource = R.drawable.f17;
                break;
            case 18:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f18);
                currentImageResource = R.drawable.f18;
                break;
            case 19:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f19);
                currentImageResource = R.drawable.f19;
                break;
            case 20:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f20);
                currentImageResource = R.drawable.f20;
                break;
            case 21:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f21);
                currentImageResource = R.drawable.f21;
                break;
            case 22:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f22);
                currentImageResource = R.drawable.f22;
                break;
            case 23:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f23);
                currentImageResource = R.drawable.f23;
                break;
            case 24:
                //view.setBackgroundResource(R.drawable.frame_13);

                changeImage(currentImageResource, R.drawable.f24);
                currentImageResource = R.drawable.f24;
                break;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_alarm, container, false);
        imageView1 = (ImageView) v.findViewById(R.id.imageView1);
        currentImageResource = R.drawable.f12;
        timePrompt = (TimePicker) v.findViewById(R.id.timeprompt);
        timePrompt.setIs24HourView(DateFormat.is24HourFormat(getActivity()));
        updateDisplay(timePrompt, currentHour, currentMinute);
        timePrompt.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                updateDisplay(view, hourOfDay, minute);
            }
        });

        startAlarmButton = (Button) v.findViewById(R.id.alarm_btn);

        startAlarmButton.setOnClickListener(this);

        spinner = (Spinner) v.findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                profileName = parent.getItemAtPosition(position).toString();
                profileChoice = profiles.get(profileName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner.setPrompt("PROMPT");


        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        //categories.add("Default Profile");
        categories.add("Walk around");
        categories.add("Shine light on me");
        categories.add("Shake it up, Baby");
        categories.add("Hold over");

        profiles.put("Walk around", "step");
        profiles.put("Shine light on me", "light");
        profiles.put("Shake it up, Baby", "shake");
        profiles.put("Hold over", "dark");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setPrompt("Select Profile");
        return v;
    }
    // TODO: Rename method, update argument and hook method into UI event

    private void changeImage(int from, int to) {
        Drawable backgrounds[] = new Drawable[2];
        backgrounds[0] = getResources().getDrawable(from);
        backgrounds[1] = getResources().getDrawable(to);

        TransitionDrawable crossfader = new TransitionDrawable(backgrounds);

        imageView1.setImageDrawable(crossfader);

        crossfader.startTransition(400);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.alarm_btn:
                Intent intent = new Intent(getActivity(), SetAlarmActivity.class);
                intent.putExtra("hour", hourChoice);
                intent.putExtra("minute", minuteChoice);
                intent.putExtra("profile", profileChoice);
                intent.putExtra("name", profileName);
                startActivityForResult(intent, 42);
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 42) {

            int id = data.getIntExtra("id", -1);
            String profile = data.getStringExtra("profile");
            String text = data.getStringExtra("text");
            int hour = data.getIntExtra("hour", -1);
            int minute = data.getIntExtra("minute", -1);
            listAlarmFragment.addAlarm(text, profile, id, hour, minute);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
