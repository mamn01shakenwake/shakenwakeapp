package com.example.lindg.shakenwake.alarm;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.lindg.shakenwake.CenteredToast;
import com.example.lindg.shakenwake.ListAlarmFragment;
import com.example.lindg.shakenwake.R;
import com.example.lindg.shakenwake.TinyDB;

import java.util.Calendar;

/**
 * Used to add/remove alarms from ListAlarmFragments view
 */
public class AlarmAdapter extends ArrayAdapter<Alarm> {
    private Context context;
    private int layoutResourceId;
    private Alarm alarm;

    public AlarmAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId, ListAlarmFragment.alarmList);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        AlarmHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new AlarmHolder();
            holder.txtTitle = (TextView) row.findViewById(android.R.id.text1);
            holder.txtSubTitle = (TextView) row.findViewById(android.R.id.text2);
            holder.userText = (TextView) row.findViewById(R.id.userText);
            holder.mySwitch = (Switch) row.findViewById(R.id.mySwitch);

            row.setTag(holder);
        } else {
            holder = (AlarmHolder) row.getTag();
            //not super efficient but works
            //http://stackoverflow.com/questions/11190390/checking-a-checkbox-in-listview-makes-other-random-checkboxes-checked-too
            holder.mySwitch.setOnCheckedChangeListener(null);
        }

        alarm = ListAlarmFragment.alarmList.get(position);
        holder.txtTitle.setText(alarm.toString());
        holder.txtSubTitle.setText(alarm.getProfile());
        holder.userText.setText(alarm.getText());
        holder.mySwitch.setChecked(alarm.isActive());

        holder.mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onSwitchActive(position);
                } else {
                    onSwitchInactive(position);
                }
            }
        });

        return row;
    }

    private void onSwitchActive(int position) {
        Alarm alarm = ListAlarmFragment.alarmList.get(position);
        alarm.setActive(true);
        ListAlarmFragment.alarmList.set(position, alarm);
        TinyDB db = new TinyDB(getContext());
        db.putListObject("alarms", ListAlarmFragment.alarmList);
        new CenteredToast(context, alarm.toString() + " active");
//        notifyDataSetChanged();
    }

    private void onSwitchInactive(int position) {
        Alarm alarm = ListAlarmFragment.alarmList.get(position);
        alarm.setActive(false);
        TinyDB db = new TinyDB(getContext());
        ListAlarmFragment.alarmList.set(position, alarm);
        db.putListObject("alarms", ListAlarmFragment.alarmList);
        new CenteredToast(context, alarm.toString() + " inactive");
//        notifyDataSetChanged();
    }

    private long getAlarmTime(int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        return calendar.getTimeInMillis();
    }

    static class AlarmHolder {
        TextView txtTitle;
        TextView txtSubTitle;
        TextView userText;
        Switch mySwitch;
    }
}
