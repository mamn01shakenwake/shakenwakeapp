package com.example.lindg.shakenwake;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lindg.shakenwake.alarm.AlarmReceiver;

public class StepActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private boolean activityRunning, start;
    private float initial;
    private int alarmID;
    private SoundPool soundPool;
    private int STEP_THRESHHOLD;
    private Intent intent;
    private Window window;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        start = true;
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        alarmID = soundPool.load(this, R.raw.siren, 1);
        STEP_THRESHHOLD = 10;

        intent = getIntent();
        window = getWindow();

        //Unlock phone, turn on screen during alarms
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        SystemClock.sleep(500);
        soundPool.play(alarmID, 1, 1, 1, -1, 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityRunning = true;
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Toast.makeText(this, "Count sensor not available!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityRunning = false;
        // if you unregister the last listener, the hardware will stop detecting step events
//        sensorManager.unregisterListener(this);
    }

    public void onSensorChanged(SensorEvent event) {
        if (activityRunning) {
            // TYPE_STEP_COUNTER only resets when the device reboots.
            // This displays the correct value no matter what
            if (start) {
                start = false;
                initial = event.values[0];

            }
            if (event.values[0] - initial > STEP_THRESHHOLD) {
                soundPool.pause(alarmID);
                soundPool.release();
                AlarmReceiver.completeWakefulIntent(intent);
                finish();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}