package com.example.lindg.shakenwake;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ShakeCounter extends AppCompatActivity implements SensorEventListener {

    private final float SHAKE_THRESHHOLD = 400;
    private final double SENSOR_THRESHHOLD = 7;
    private final float alph = 0.8f;
    public SensorManager mSensorManager;
    public Sensor Accelerometer;
    protected float[] gravSensorVals;
    TextView textView;
    private long lastDetection;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shake_counter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lastDetection = System.currentTimeMillis();
        count = 0;

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
        count = 0;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void onSensorChanged(SensorEvent event) {

        if (System.currentTimeMillis() - lastDetection > SHAKE_THRESHHOLD) {
            //float acc_x = event.values[0];
            //float acc_y = event.values[1];
            //float acc_z = event.values[2];
            double[] accVals = new double[3];

            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                gravSensorVals = lowPass(event.values.clone(), gravSensorVals);
            }

            accVals[0] = event.values[0] - gravSensorVals[0];
            accVals[1] = event.values[1] - gravSensorVals[1];
            accVals[2] = event.values[2] - gravSensorVals[2];

            double speed = Math.sqrt(Math.pow(accVals[0], 2) + Math.pow(accVals[1], 2) + Math.pow(accVals[2], 2));


            if (speed > SENSOR_THRESHHOLD) {
                count++;

                lastDetection = System.currentTimeMillis();
            }

        } else {
            return;
        }
    }

    protected float[] lowPass(float[] input, float[] output) {
        if (output == null) return input;
        for (int i = 0; i < input.length; i++) {
            output[i] = alph * output[i] + (1 - alph) * input[i];
        }
        return output;
    }
}
