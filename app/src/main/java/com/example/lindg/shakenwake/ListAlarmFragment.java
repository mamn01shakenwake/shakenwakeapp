package com.example.lindg.shakenwake;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.lindg.shakenwake.alarm.Alarm;
import com.example.lindg.shakenwake.alarm.AlarmAdapter;

import java.util.ArrayList;
import java.util.Random;

public class ListAlarmFragment extends Fragment {

    public static ArrayList<Alarm> alarmList;
    private ListView list;
    private AlarmAdapter adapter;
    private TinyDB db;

    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            final Alarm alarm = alarmList.get(position);
            final EditText editText = new EditText(getContext());
            editText.setPadding(dpToPx(10), 0, 0, dpToPx(15));
            editText.setSingleLine();
            editText.setHint("Alarm name");
            editText.setText(alarm.getText());
            editText.setSelection(editText.getText().length());
            editText.append("");
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(dpToPx(20), dpToPx(15), dpToPx(20), 0);

            LinearLayout layout = new LinearLayout(getContext());
            layout.addView(editText, params);

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setView(layout);
            builder.setTitle(alarm.toString());
            builder.setNeutralButton("Cancel", null);
            builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    AlertDialog.Builder confirmDeleteBuilder = new AlertDialog.Builder(getContext());
                    confirmDeleteBuilder.setTitle("Delete alarm " + alarm.getText());
                    confirmDeleteBuilder.setMessage("Are you sure?");
                    confirmDeleteBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            onDelete(position);
                        }
                    });
                    confirmDeleteBuilder.setNegativeButton("Cancel", null);
                    AlertDialog confirmDeleteDialog = confirmDeleteBuilder.show();
                    confirmDeleteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
                }
            });
            builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    new CenteredToast(getContext(), "Alarm renamed to " +
                            editText.getText().toString());
                    alarm.setText(editText.getText().toString());
                    alarmList.set(position, alarm);
                    db.putListObject("alarms", alarmList);
                    adapter.notifyDataSetChanged();
                }
            });

            AlertDialog dialog = builder.show(); //returns alertdialog
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
            dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.GRAY);
        }
    };

    public ListAlarmFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (db == null) {
            db = new TinyDB(getContext());
        }
        ArrayList<Alarm> prefList = db.getListObject("alarms", Alarm.class);
        if (prefList != null) {
            alarmList = prefList;
        } else {
            alarmList = new ArrayList<Alarm>();
        }
        if (adapter == null) {
            adapter = new AlarmAdapter(getContext(), R.layout.listview_item_row);
        }
    }

    private int dpToPx(int dp) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_alarm, container, false);

        list = (ListView) v.findViewById(R.id.list_alarm);
        list.setAdapter(adapter);
        list.setOnItemClickListener(onListClick);




        return v;
    }

    private void onDelete(int position) {
        new CenteredToast(getActivity(), "Deleted");
        alarmList.remove(position);
        adapter.notifyDataSetChanged();
        db.remove("alarms");
        db.putListObject("alarms", alarmList);
    }


    public void addAlarm(String text, String profile, int id, int hour, int minute) {
        Alarm alarm = new Alarm(hour, minute, id, profile, text);
        alarmList.add(alarm);
        db.putListObject("alarms", alarmList);
        adapter.notifyDataSetChanged();
    }
}