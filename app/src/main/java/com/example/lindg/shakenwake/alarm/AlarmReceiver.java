package com.example.lindg.shakenwake.alarm;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.example.lindg.shakenwake.DarkActivity;
import com.example.lindg.shakenwake.LightActivity;
import com.example.lindg.shakenwake.ListAlarmFragment;
import com.example.lindg.shakenwake.SoundActivity;
import com.example.lindg.shakenwake.StepActivity;
import com.example.lindg.shakenwake.TinyDB;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class AlarmReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        String profile = intent.getStringExtra("profile");
        Intent alarmIntent;
        switch (profile) {
            case "shake":
                alarmIntent = new Intent(context, SoundActivity.class);
                break;
            case "light":
                alarmIntent = new Intent(context, LightActivity.class);
                break;
            case "step":
                alarmIntent = new Intent(context, StepActivity.class);
                break;
            case "dark":
                alarmIntent = new Intent(context, DarkActivity.class);
                break;
            default:
                alarmIntent = new Intent(context, SoundActivity.class);
                break;
        }
        //MUST be included when intent called outside activity
        alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //To overlay alarm dialog over other apps
        alarmIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
//        startWakefulService(context, i);
        int id = intent.getIntExtra("id", -1);
        TinyDB db = new TinyDB(context);
        ArrayList<Alarm> alarmList = db.getListObject("alarms", Alarm.class);
        for (int i = 0; i < alarmList.size(); i++) {
            Alarm alarm = alarmList.get(i);
            //TODO check and handle repeating
            if (alarm.getID() == id && alarm.isActive()) {
                context.startActivity(alarmIntent);
            }
        }
    }
}