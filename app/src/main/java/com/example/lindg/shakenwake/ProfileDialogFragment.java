package com.example.lindg.shakenwake;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileDialogFragment extends DialogFragment {

    private int defaultValue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profiledialog, container, false);
        getDialog().setTitle("Profile Dialog");

        Bundle bundle = this.getArguments();
        int counter = bundle.getInt("key", defaultValue);
        TextView beskr = (TextView) rootView.findViewById(R.id.beskr);
        ImageView bild = (ImageView) rootView.findViewById(R.id.image);

        if (counter==0){
            beskr.setText("Good morning, sir! Start your day by greeting your phone with a couple of handshakes.");
            bild.setImageResource(R.drawable.handshake);
        }
        if (counter==1){
            beskr.setText("Brains... Rise from the dead and walk to shut the alarm off.");
            bild.setImageResource(R.drawable.walker);
        }
        if (counter==2){
            beskr.setText("Let there be light! Shine light on your phone to silence it.");
            bild.setImageResource(R.drawable.sunshine);
        }
        if (counter==3) {
            beskr.setText("It's too bright! Put your phone to sleep again by covering it.");
            bild.setImageResource(R.drawable.darkness);
        }
        if (counter==4){
            beskr.setText("Get up, maggot! Start your day with shakes and marching.");
            bild.setImageResource(R.drawable.drillsergeant);
        }
        if (counter==5){
            beskr.setText("Prepare to be punished. You will have to shake, walk, cover and shine on your phone to escape.");
            bild.setImageResource(R.drawable.thepunisher);
        }

        Button dismiss = (Button) rootView.findViewById(R.id.dismiss);
        dismiss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return rootView;
    }
}
