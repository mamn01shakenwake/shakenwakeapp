package com.example.lindg.shakenwake;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private Toolbar toolbar;
    private Button btnGUI, btnShake, btnSound, btnTestAlarm, btnStep, btnLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnGUI = (Button) findViewById(R.id.btnIconTextTabs);
        btnShake = (Button) findViewById(R.id.btnScrollableTabs);
        btnSound = (Button) findViewById(R.id.SoundButton);
        btnTestAlarm = (Button) findViewById(R.id.btnTestAlarm);
        btnStep = (Button) findViewById(R.id.btnStep);
        btnLight = (Button) findViewById(R.id.btnLight);

        btnGUI.setOnClickListener(this);
        btnShake.setOnClickListener(this);
        btnSound.setOnClickListener(this);
        btnTestAlarm.setOnClickListener(this);
        btnStep.setOnClickListener(this);
        btnLight.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnIconTextTabs:
                startActivity(new Intent(MainActivity.this, IconTextTabsActivity.class));
                break;
            case R.id.btnScrollableTabs:
                startActivity(new Intent(MainActivity.this, ShakeCounter.class));
                break;
            case R.id.SoundButton:
                startActivity(new Intent(MainActivity.this, SoundActivity.class));
                break;
            case R.id.btnStep:
                startActivity(new Intent(MainActivity.this, StepActivity.class));
                break;
            case R.id.btnLight:
                startActivity(new Intent(MainActivity.this, LightActivity.class));
                break;
        }
    }
}
