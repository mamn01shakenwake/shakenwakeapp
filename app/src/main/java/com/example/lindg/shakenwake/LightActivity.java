package com.example.lindg.shakenwake;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.lindg.shakenwake.alarm.AlarmReceiver;

public class LightActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor LightSensor;
    private TextView light;
    private boolean start;
    private float initial;
    private int alarmID;
    private SoundPool soundPool;
    private Intent intent;
    private Window window;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        light = (TextView) findViewById(R.id.light);
        start = true;
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        LightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        alarmID = soundPool.load(this, R.raw.siren, 1);

        intent = getIntent();
        window = getWindow();

        //Unlock phone, turn on screen during alarms
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        SystemClock.sleep(500);
        soundPool.play(alarmID, 1, 1, 1, -1, 1);
    }

    @Override
    protected void onResume() {
        sensorManager.registerListener(this, LightSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        super.onResume();
    }

    @Override
    protected void onPause() {
        sensorManager.unregisterListener(this);
        super.onPause();
        // if you unregister the last listener, the hardware will stop detecting step events
//        sensorManager.unregisterListener(this);
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {

            if (start) {
                start = false;
                initial = event.values[0];
            }
            if (event.values[0] > initial + 10) {
                soundPool.pause(alarmID);
                soundPool.release();
                AlarmReceiver.completeWakefulIntent(intent);
                finish();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}