package com.example.lindg.shakenwake.alarm;

public class Alarm {
    private boolean active;
    private int hour, minute, id;
    private String profile, text = "";

    public Alarm(int hour, int minute, int id, String profile, String text) {
        this.hour = hour;
        this.minute = minute;
        this.profile = profile;
        this.text = text;
        this.id = id;
        this.active = true;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean status) {
        active = status;
    }

    public void setTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String toString() {
        return String.format("%02d", hour) + ":" + String.format("%02d", minute);
    }

    public String getText() {
        return text;
    }

    public String getProfile() {
        return profile;
    }

    public int getID() {
        return id;
    }
}
