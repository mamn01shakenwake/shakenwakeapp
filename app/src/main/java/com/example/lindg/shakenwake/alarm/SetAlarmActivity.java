package com.example.lindg.shakenwake.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.lindg.shakenwake.CenteredToast;
import com.example.lindg.shakenwake.IconTextTabsActivity;
import com.example.lindg.shakenwake.R;
import com.example.lindg.shakenwake.TinyDB;

import java.util.Calendar;

public class SetAlarmActivity extends AppCompatActivity {

    public static AlarmManager alarmManager;
    public static TinyDB db;
    private PendingIntent pendingIntent;
    private String profile, name, text;
    private int hour, minute, id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_set);

        Intent intent = getIntent();

        profile = intent.getStringExtra("profile");
        name = intent.getStringExtra("name");
        hour = intent.getIntExtra("hour", 1);
        minute = intent.getIntExtra("minute", 1);

        if (alarmManager == null) {
            alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        }
        if (db == null) {
            db = new TinyDB(this);
        }

        Intent i = new Intent(getApplicationContext(), AlarmReceiver.class);
        // Make intent unique, comparison purposes
        i.setData(Uri.parse(Integer.toString(i.hashCode())));
        // Input alarm details
        i.putExtra("profile", profile);
        i.putExtra("hour", hour);
        i.putExtra("minute", minute);
        id = i.hashCode();
        i.putExtra("id", id);

        Intent callback = new Intent(getApplicationContext(), IconTextTabsActivity.class);
        callback.putExtra("text", name);
        callback.putExtra("id", id);
        callback.putExtra("profile", profile);
        callback.putExtra("hour", hour);
        callback.putExtra("minute", minute);


        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), id, i, 0);
        String hourText = String.format("%02d", hour);
        String minuteText = String.format("%02d", minute);
        text = "Alarm \"" + name + "\" has been set at " + hourText + ":" + minuteText;
        setAlarm();
        new CenteredToast(getApplicationContext(), text);
        setResult(1, callback);
        finish();
    }

    private void setAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        if (Calendar.getInstance().getTimeInMillis() - calendar.getTimeInMillis() > 0) {
            calendar.add(Calendar.DATE, 1);
            text += " tomorrow";
        }
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }
}